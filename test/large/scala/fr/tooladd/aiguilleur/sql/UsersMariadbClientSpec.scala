/**
  * Copyright (C) 2019 Tool-Add
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
  */

package fr.tooladd.aiguilleur.sql

import java.time.ZonedDateTime

import akka.actor.ActorSystem
import akka.testkit.TestKit
import com.typesafe.config.ConfigFactory
import fr.tooladd.aiguilleur.model.{GetUsers, User}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{AsyncWordSpecLike, Matchers}

class UsersMariadbClientSpec
  extends TestKit(ActorSystem("UsersMariadbClientSpec"))
    with AsyncWordSpecLike
    with Matchers
    with ScalaFutures {

  val conf = ConfigFactory.load().getConfig("aiguilleur.mariadb-users.service-conf")
  val usersClient = SqlServiceClient(conf)

  "UsersMariadbClient" should {

    "return valid object in response for a basic request" in {

      val userReq = GetUsers()

      usersClient.singleRequest(userReq) map {
        userList => {
          assert(userList.length >= 5)
          userList.head shouldEqual User(
            10,
            "John Doe",
            "john.doe@doe_it_yourself.fr",
            ZonedDateTime.parse("2012-12-21T12:00Z[Etc/UTC]"),
            ZonedDateTime.parse("2012-12-21T12:00Z[Etc/UTC]"),
            Some(false)
          )
        }
      }
    }
  }

}
