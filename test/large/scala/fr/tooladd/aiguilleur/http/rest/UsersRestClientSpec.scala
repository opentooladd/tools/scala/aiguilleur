/**
  * Copyright (C) 2019 Tool-Add
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
  */

package fr.tooladd.aiguilleur.http.rest

import java.time.ZonedDateTime

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.testkit.TestKit
import com.typesafe.config.ConfigFactory
import fr.tooladd.aiguilleur.http.HttpServiceClient
import fr.tooladd.aiguilleur.model.{GetUsers, User}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{AsyncWordSpecLike, Matchers}

class UsersRestClientSpec
  extends TestKit(ActorSystem("UsersRestClientSpec"))
    with AsyncWordSpecLike
    with Matchers
    with ScalaFutures {

  implicit val mat = ActorMaterializer()

  val conf = ConfigFactory.load().getConfig("aiguilleur.rest-users.service-conf")
  val usersClient = HttpServiceClient(conf)

  "UsersClient" should {

    "return valid object in response for a basic request" in {
      val userReq = GetUsers()

      usersClient.singleRequest(userReq) map {
        userList => {
          assert(userList.length >= 4)
          userList.head shouldEqual User(
            10,
            "John Doe",
            "john.doe@doe_it_yourslef.fr",
            ZonedDateTime.parse("2012-12-21T12:00Z"),
            ZonedDateTime.parse("2012-12-21T12:00Z"),
            None
          )
        }
      }
    }
  }

}
