/**
  * Copyright (C) 2019 Tool-Add
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
  */

package fr.tooladd.aiguilleur.model

import java.time.ZonedDateTime

import akka.http.scaladsl.model.{HttpEntity, HttpMethods, HttpResponse, HttpRequest => AkkaHttpRequest}
import fr.tooladd.aiguilleur.Aiguilleur
import fr.tooladd.aiguilleur.http.{HttpRequest, HttpServiceConnector, HttpServiceDescriber}
import fr.tooladd.aiguilleur.sql.{SqlRequest, SqlServiceConnector, SqlServiceDescriber}
import io.circe._
import io.circe.generic.semiauto._
import io.circe.parser._
import scalikejdbc.{NoExtractor, SQL}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}


case class User(
  id: Int,
  name: String,
  email: String,
  created_at: ZonedDateTime,
  updated_at: ZonedDateTime,
  deleted: Option[Boolean],
)

case object User {
  implicit val decoder: Decoder[User] = deriveDecoder
  implicit val encoder: Encoder[User] = deriveEncoder
}

case class GetUsers( id: Option[String] = None ) extends HttpRequest with SqlRequest

case object GetUsers {

  implicit val decoder: Decoder[GetUsers] = deriveDecoder
  implicit val encoder: Encoder[GetUsers] = deriveEncoder

  implicit val restConnector: HttpServiceConnector[GetUsers, List[User]] =
    new HttpServiceConnector[GetUsers, List[User]] {

      // TODO : find a way to use client dedicated materializer
      implicit val mat = Aiguilleur.materializer
      implicit val ex = mat.executionContext

      def buildServiceRequest(describer: HttpServiceDescriber, req: GetUsers): AkkaHttpRequest = {
        val uri = describer.getPath("users/")
        AkkaHttpRequest(HttpMethods.GET, uri = uri)
      }

      def handleServiceResponse(
        describer: HttpServiceDescriber,
        httpResponse: HttpResponse,
        req: GetUsers
      ): List[User] = {

        // @TODO use configuration for connection timeout depending on service
        val connectionTimeout = 5 seconds

        val futureStrictResponse: Future[HttpEntity.Strict] = httpResponse.entity.toStrict(connectionTimeout)

        val futureResult = futureStrictResponse.map {
          entity => {
            // get string response
            val jsonStr = entity.data.utf8String
            val decoded = decode[List[User]](jsonStr)

            decoded match {
              case Right(r) => r
              case Left(e) => {
                error(e.toString)
                ??? // error found; TODO : define generic error management
              }
            }
          }
        }

        // TODO : find a generic way to handle service responses
        Await.result(futureResult, 5 seconds)
      }

      def handleServiceResponse(
        describer: HttpServiceDescriber,
        fHttpResponse: Future[HttpResponse], req: GetUsers
      ): Future[List[User]] = {
        fHttpResponse.map { handleServiceResponse(describer, _, req) }
      }
  }


  implicit val sqlConnector: SqlServiceConnector[GetUsers, List[User]] = new SqlServiceConnector[GetUsers, List[User]] {


    def buildServiceRequest ( describer: SqlServiceDescriber, req: GetUsers ): String = {
      // TODO : enhance dynamic SQL query creation
      "SELECT * FROM aiguilleur_test.user;"
    }

    def handleServiceResponse( describer: SqlServiceDescriber, sqlResponse: SQL[Nothing, NoExtractor], req: GetUsers )
    : List[User] = {

      // TODO : this is necessary for apply() implicits => understand hasExtractor usage to remove this useless implicit usage
      implicit val session = describer.session

      sqlResponse.map(rs => {
        new User(
          rs.int("id"),
          rs.string("name"),
          rs.string("email"),
          rs.zonedDateTime("created_at"),
          rs.zonedDateTime("updated_at"),
          rs.booleanOpt("deleted"),
        )
      }).list.apply()
    }

    // TODO : manage this method in abstract Connector
    def handleServiceResponse( describer: SqlServiceDescriber, sqlResponse: Future[SQL[Nothing, NoExtractor]], req: GetUsers )
    : Future[List[User]] = {
      ???
    }
  }

}
