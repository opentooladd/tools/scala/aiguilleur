/**
  * Copyright (C) 2019 Tool-Add
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
  */

package fr.tooladd.aiguilleur

import akka.actor.ActorSystem
import akka.testkit.TestKit
import com.typesafe.config.ConfigFactory
import fr.tooladd.aiguilleur.http.HttpServiceClient
import fr.tooladd.aiguilleur.sql.SqlServiceClient
import org.scalatest.{Matchers, WordSpecLike}

class AiguilleurSpec
  extends TestKit(ActorSystem("AiguilleurSpec"))
    with WordSpecLike
    with Matchers {

  Aiguilleur.system = system

  "Aiguilleur" should {

    "return proper client according given configuration" in {

      val rest_conf = ConfigFactory.load().getConfig("aiguilleur.rest-client")
      assert(Aiguilleur(rest_conf).isInstanceOf[HttpServiceClient] )

      val sql_conf = ConfigFactory.load().getConfig("aiguilleur.mariadb-users")
      assert(Aiguilleur(sql_conf).isInstanceOf[SqlServiceClient] )

    }
  }

}
