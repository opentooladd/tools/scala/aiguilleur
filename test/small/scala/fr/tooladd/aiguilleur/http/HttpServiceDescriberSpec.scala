/**
  * Copyright (C) 2019 Tool-Add
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
  */

package fr.tooladd.aiguilleur.http

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.testkit.TestKit
import com.typesafe.config.ConfigFactory
import org.scalatest.{Matchers, WordSpecLike}

class HttpServiceDescriberSpec
  extends TestKit(ActorSystem("HttpServiceDescriberSpec"))
    with WordSpecLike
    with Matchers {

  implicit val mat = ActorMaterializer()

  val conf = ConfigFactory.load().getConfig("aiguilleur")

  "HttpServiceDescriber" should {

    "return proper path for given configuration" in {
      val restDescriber = HttpServiceDescriber(conf.getConfig("rest-client.service-conf"))
      restDescriber.getPath("my/resource/path") equals "http://tool-add.fr/my/resource/path"

      val restWithoptDescriber = HttpServiceDescriber(conf.getConfig("rest-client-withopt.service-conf"))
      restWithoptDescriber.getPath("my/resource/path") equals "https://tool-add.fr:2121/api/my/resource/path"
    }
  }

}
