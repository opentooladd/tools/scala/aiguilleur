/**
  * Copyright (C) 2019 Tool-Add
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
  */

package fr.tooladd.aiguilleur.http

import akka.actor.ActorSystem
import akka.testkit.TestKit
import org.scalatest.{Matchers, WordSpecLike}

class HttpServiceClientSpec
  extends TestKit(ActorSystem("HttpServiceClientSpec"))
    with WordSpecLike
    with Matchers {

//  implicit val mat = ActorMaterializer()

  "HttpServiceClientSpec" should {

    "use proper proxy setup according given configuration" in {
      pending
    }

    "use proper ssl setup according given configuration" in {
      pending
    }
  }

}
