# Aiguilleur

[![pipeline status](https://gitlab.com/opentooladd/aiguilleur/badges/master/pipeline.svg)](https://gitlab.com/opentooladd/aiguilleur/commits/master)


This library define elementary and generic interface
and tools to manage any kind of request.

It offers some useful classes and traits to define REST, GraphQL, SQL, ... APIs clientwant s


## Overall view

For a given service, you have to use a corresponding `AiguilleurServiceClient` that is properly 
configured to request your service instance.

For each business request you have to manage, you have to implement
* the result data schema
* the request schema (extending `AiguilleurRequest`)
* a connector (extending `AiguilleurServiceConnector`)
 
The connector has responsibility of business/service translations. It translates defined business request 
into proper service request and service response into proper business object.

Then usage is really simple : once your business request is properly defined, 
you simply have to pass it to `poll(...)` or `singleRequest(...)` methods of the client
with proper connector defined as implicit and it's done.


## Usages examples


### basic usage

```scala
import akka.http.scaladsl.model.{HttpEntity, HttpMethods, HttpRequest => AkkaHttpRequest, HttpResponse}
import akka.stream.Materializer
import fr.tooladd.aiguilleur.http._

import scala.concurrent.Future
import scala.concurrent.duration._


// first define your business objects and requests.
///////////////////////////////////////////////////////

case class BizElement(
   id: Int,
   name: String
)

case class GetElement( id: Option[Int] ) extends HttpRequest


case object GetElement {

  implicit val restConnector: HttpServiceConnector[GetElement, List[BizElement]] =
  new HttpServiceConnector[GetElement, List[BizElement]] {

      def buildServiceRequest(describer: HttpServiceDescriber, req: GetElement): AkkaHttpRequest = ???

      def handleServiceResponse(
        describer: HttpServiceDescriber,
        httpResponse: HttpResponse,
        req: GetElement
      ): List[BizElement] = ???

      def handleServiceResponse(
        describer: HttpServiceDescriber,
        fHttpResponse: Future[HttpResponse], req: GetElement
      ): Future[List[BizElement]] = ???

    def buildHttpRequest(client: HttpServiceClient, req: GetElement): HttpRequest = ???

    def parseHttpResponse(
      httpResponse: HttpResponse,
      req: GetElement
    )( implicit
      mat: Materializer
    ): Future[List[BizElement]] = ???

    def parseHttpResponse(
      fHttpResponse: Future[HttpResponse], req: GetElement
    )(
      implicit
        rsc: HttpServiceConnector[GetElement, List[BizElement]],
        mat: Materializer
    ): Future[List[BizElement]] = ???
  }

}


// once properly implemented, you can use your requests
///////////////////////////////////////////////////////

import com.typesafe.config.ConfigFactory

// define your client
val conf = ConfigFactory.load().getConfig("aiguilleur")
val bizClient = HttpServiceClient(conf)

// define your contextual request
val bizReq = GetElement(id = Some(21))

// execute request
bizClient.singleRequest(bizReq) map {
  bizEltList => ???
}
```

### rest client configuration example :

TODO : this parts isn't up to date (required tests and refactoring)

> this configuration is overwritten from akka client default config
>
> https://lightbend.github.io/ssl-config/WSQuickStart.html

```conf
ssl-config {

    disabledKeyAlgorithms = ["RSA keySize < 1024"]

    keyManager = {
        stores = [
            {
                type: "pkcs12",
                path: "keystore.p12",
                password: "password1"
            }
        ]
    }

    // please notice that if no value given it will ensure default akka / java store management.
    // proper configuration format is 'stores = [ { type = "JKS", path = "{my_cert_path}/cacerts" } ]'
    // you can consider this config as an environment sub config
    trustManager = {
      stores = 'stores = [ { type = "JKS", path = "{my_cert_path}/cacerts" } ]'
    }

    // if no proxy given, take jvm defined proxy, if not defined or null values, don't use proxy
    proxy {
        host = localhost
        port = 3128
    }
}
```

### Custom usage of HttpServiceClient

TODO : describe how to use custom client configuration.


## Tests

We use testing model based on [Test Size](https://testing.googleblog.com/2010/12/test-sizes.html)

In order to run,
* all tests : ```make test-full```
* small tests : ```make test-small```
* medium tests : ```make test-medium```
* large tests : ```make test-large```


> remember that you have to run ```make start``` your docker environment
> before to test medium and large tests.


## Development & Deployment

Before to start development, you have to run `git config --local include.path ../.gitconfig`

> It ensures that restbird users data files are not committed for each "lasttime" modification


We hardly recommend to develop this library according
[TDD](https://en.wikipedia.org/wiki/Test-driven_development) strategy.

If you need some specific development to test a custom development, you may enjoy this :
1. Choose version in ```{repository}/build.sbt```
file and adapt your own project dependency.
Take a look at [sbt documentation](https://www.scala-sbt.org/1.0/docs/Publishing.html)
2. Run ```sbt publishLocal``` in order to publish this lib locally


## TODO

* review generic request management.
    > We should dissociate content / filter / pagination / sort information
    > create 3 default clients : rest / graphql / sql
* manage polling
* make things generic enough to build client / server definition according same generic business description
* review version management.
    > create environment variable for local development
* look at a factory for ```HttpServiceConnector``` definition depending on Config "api-type" value
* setup project to define `core`, `rest`, `sql`, ... packages.
    * the goal is to allow library user to use only what they need.
* publish the library on maven repository
    * https://www.scala-sbt.org/release/docs/Publishing.html
    * https://www.scala-sbt.org/1.x-beta/docs/Using-Sonatype.html
* enhance documentation and examples
    * add / enhance gitlab badges
        * https://docs.gitlab.com/ee/user/project/badges.html
        * https://gitlab.com/help/user/project/pipelines/settings.md#test-coverage-report-badge
* contributors file
* review gitlabci install process 
    * https://docs.gitlab.com/ee/user/project/container_registry.html
    * create docker image for gitlabCI (CI should not take 10 minutes)
    * make docker image usage consistent with development environment
    * improve CI tests duration
* Find a way to not stop tests (`make test-full`) when one of small/medium/large fail but ensure gitlabCI consider job failed.
* [database access configuration](http://scalikejdbc.org/documentation/configuration.html)

## development plan :

* 0.0.1
  * first version for proof of concept and project basics (setup / tests / phylosophy / ...)
* 1.0.0
  * complete SQL/REST clients, describers
  * dissociate packages 
  * raw requests
* 2.0.0
  * require more case studies
  * enhance abstraction
  * offer code tools, traits, ... to make request construction easier
* 3.0.0
  * allow server construction from object definition
