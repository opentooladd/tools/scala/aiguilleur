object Dependencies {

//  val logBackVersion = "1.2.3"
  val akkaVersion = "2.5.17"
  val akkaHttpVersion = "10.1.5"
  val circeVersion = "0.10.0"
}
