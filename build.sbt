import Dependencies._

name := "aiguilleur"

organization := "fr.tool-add"
version := "0.0.1"
scalaVersion := "2.12.8"

//target := baseDirectory.value / "infra/tmp/target"
//crossTarget := baseDirectory.value / "infra/tmp/crossTarget"

scalaSource in Compile := baseDirectory.value / "src/scala"


lazy val LargeTest = config("large") extend Test
lazy val MediumTest = config("medium") extend Test
lazy val SmallTest = config("small") extend Test

lazy val root = (project in file("."))
  .configs(LargeTest)
  .configs(MediumTest)
  .configs(SmallTest)
  .settings(

    inConfig(LargeTest)(Defaults.testSettings),
    scalaSource in LargeTest := baseDirectory.value / "test/large/scala",
    resourceDirectory in LargeTest := baseDirectory.value / "test/large/resources",

    inConfig(MediumTest)(Defaults.testSettings),
    scalaSource in MediumTest := baseDirectory.value / "test/medium/scala",
    resourceDirectory in MediumTest := baseDirectory.value / "test/medium/resources",

    inConfig(SmallTest)(Defaults.testSettings),
    scalaSource in SmallTest := baseDirectory.value / "test/small/scala",
    resourceDirectory in SmallTest := baseDirectory.value / "test/small/resources",

    libraryDependencies ++= Seq(

      // Parsing
      "io.circe" %% "circe-core" % circeVersion,
      "io.circe" %% "circe-generic" % circeVersion,
      "io.circe" %% "circe-parser" % circeVersion,

      // Rest API access
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-actor" % akkaVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      "com.typesafe.akka" %% "akka-slf4j" % "2.5.17",

      // SQL database access
      "org.scalikejdbc" %% "scalikejdbc"        % "3.3.+",
      "com.h2database" %  "h2"                 % "1.4.+",

      // Logs
      "org.wvlet.airframe" %% "airframe-log" % "19.4.1",
//      "com.outr" %% "scribe" % "2.7.2",

      // Tests
      "org.scalatest" %% "scalatest" % "3.0.5" % "large,medium,small",
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % "large,medium,small",
      "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % "large,medium,small",
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "large,medium,small",
      "org.mockito" % "mockito-core" % "2.23.0" % "large,medium,small",

      // Database tests
      "org.mariadb.jdbc" % "mariadb-java-client" % "1.1.7" % "large,medium,small",
      "ch.qos.logback"  %  "logback-classic"    % "1.2.+" % "large,medium,small",
    )
  )
