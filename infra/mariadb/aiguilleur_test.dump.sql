DROP PROCEDURE IF EXISTS PROC_DROP_CONSTRAINT;
DELIMITER $$
CREATE PROCEDURE PROC_DROP_CONSTRAINT(IN tableName VARCHAR(64), IN constraintName VARCHAR(64))
BEGIN
    IF EXISTS(
            SELECT * FROM information_schema.table_constraints
            WHERE
                    table_schema    = DATABASE()     AND
                    table_name      = tableName      AND
                    constraint_name = constraintName AND
                    constraint_type = 'CONSTRAINT')
    THEN
        SET @query = CONCAT('ALTER TABLE ', tableName, ' DROP FOREIGN KEY ', constraintName, ';');
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END IF;
END$$
DELIMITER ;


CALL PROC_DROP_CONSTRAINT('user_address', 'FK_BEFA93B791BD8781');
CALL PROC_DROP_CONSTRAINT('user_address', 'FK_BEFA93B79393F8FE');

DROP TABLE IF EXISTS user_address;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS address;

CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT 0 NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;
CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, road VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, country VARCHAR(14) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;
CREATE TABLE user_address (user_id INT NOT NULL, address_id INT NOT NULL, INDEX IDX_BEFA93B791BD8781 (user_id), INDEX IDX_BEFA93B79393F8FE (address_id), PRIMARY KEY(user_id, address_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;

ALTER TABLE user_address ADD CONSTRAINT FK_BEFA93B791BD8781 FOREIGN KEY (user_id) REFERENCES user (id);
ALTER TABLE user_address ADD CONSTRAINT FK_BEFA93B79393F8FE FOREIGN KEY (address_id) REFERENCES address (id);

INSERT INTO user (id, name, email, created_at, updated_at, deleted) VALUES (10, "John Doe","john.doe@doe_it_yourself.fr","2012-12-21 12:00:00","2012-12-21 12:00:00",0);
INSERT INTO user (id, name, email, created_at, updated_at, deleted) VALUES (11, "John Doe","john.doe@freedom.fr","2006-09-12 12:00:00","2006-09-12 12:00:00",1);
INSERT INTO user (id, name, email, created_at, updated_at, deleted) VALUES (12, "Dark Vador","dark.vador@death.stars.universe","2060-01-01 12:00:00","2060-01-01 12:00:00",0);
INSERT INTO user (id, name, email, created_at, updated_at, deleted) VALUES (13, "Luffy D. Monkey","monkey.d.luffy@fdn.fr","2008-09-10 12:00:00","2008-09-10 12:00:00",0);
INSERT INTO user (id, name, email, created_at, updated_at, deleted) VALUES (14, "Guy Fawkes","fawkes.guy@freedom.com","2000-01-01 00:00:01","2000-01-01 00:00:01",0);

INSERT INTO address (id, road, city, country) VALUES (20, "666", "-", "USA");
INSERT INTO address (id, road, city, country) VALUES (21, "rue du Paradis", "Bordeaux", "France");
INSERT INTO address (id, road, city, country) VALUES (22, "rue Alan Moore", "Tours", "France");
INSERT INTO address (id, road, city, country) VALUES (23, "le jardin des plumes", "Giverny", "France");
INSERT INTO address (id, road, city, country) VALUES (24, "rue du GroS Horloge", "Rouen", "France");

INSERT INTO user_address (user_id, address_id) VALUES (10, 20);
INSERT INTO user_address (user_id, address_id) VALUES (11, 20);
INSERT INTO user_address (user_id, address_id) VALUES (14, 22);
INSERT INTO user_address (user_id, address_id) VALUES (14, 23);
INSERT INTO user_address (user_id, address_id) VALUES (13, 23);
