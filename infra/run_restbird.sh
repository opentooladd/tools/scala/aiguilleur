#!/bin/sh

script_path="$( cd "$(dirname "$0")" ; pwd -P )"
cookie_path="$script_path/tmp/restbird_cookie"
log_path="$script_path/tmp/curl.log"

printf "\n\n== Run restbird\n"

echo `curl --version`

printf "\n== Login restbird server\n"
#curl --user admin:admin --cookie-jar "$cookie_path" 'http://0.0.0.0:8080/v1/base/login' --data '{"name":"admin","password":"admin"}'
curl --user admin:admin --cookie-jar "$cookie_path" 'http://0.0.0.0:8080/v1/base/login' --data '{"name":"admin","password":"admin"}' --trace-ascii "$log_path"
cat ${log_path}

printf "\n== Start user_api mock server\n"
#curl --cookie "$cookie_path" 'http://0.0.0.0:8080/v1/mock/start?project=user_api&isdebug=false'
curl --cookie "$cookie_path" 'http://0.0.0.0:8080/v1/mock/start?project=user_api&isdebug=false' --trace-ascii "$log_path"
cat ${log_path}

printf "\n\n== Restbird ready !!\n\n"
