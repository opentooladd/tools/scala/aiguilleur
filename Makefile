
## Global commands
#######################

# start docker containers; To run before any other command.
start:
	mkdir -p ./infra/tmp/
	COMPOSE_HTTP_TIMEOUT=300 docker-compose up -d
#	COMPOSE_HTTP_TIMEOUT=300 docker-compose up -d --verbose --force-recreate

# Stop containers
stop:
	docker-compose stop

down:
	docker-compose down --rmi all -v --remove-orphans

clean:
	docker-compose run scala sbt clean
	rm -rf ./infra/tmp/


## Containers access commands
##############################

shell-rest:
	docker exec -it aiguilleur-rest bash

shell-sql:
	docker exec -it aiguilleur-sql bash


## Database management commands
#################################


## Tests commands
##################

test-small:
	docker-compose run scala sbt small:test

test-medium:
	docker-compose run scala sbt medium:test

test-large:
	./infra/run_restbird.sh
	docker-compose run scala sbt large:test

test-full: test-small
test-full: test-medium
test-full: test-large
