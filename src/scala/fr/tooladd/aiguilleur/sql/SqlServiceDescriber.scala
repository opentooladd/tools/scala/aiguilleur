/**
  * Copyright (C) 2019 Tool-Add
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
  */

package fr.tooladd.aiguilleur.sql

import com.typesafe.config.Config
import fr.tooladd.aiguilleur.AiguilleurServiceDescriber
import scalikejdbc._

class SqlServiceDescriber(
  driverClass: String,
  url: String,
  user: String,
  password: String
) extends AiguilleurServiceDescriber {

  // initialize JDBC driver & connection pool
  Class.forName(driverClass)
  ConnectionPool.singleton(url, user, password)

  // ad-hoc session provider on the REPL
  val session = AutoSession

}

case object SqlServiceDescriber {
  def apply(config: Config): SqlServiceDescriber = {

    val driver = config.getString("driver")
    val url = config.getString("url")
    val user = config.getString("user")
    val password = config.getString("password")

    new SqlServiceDescriber(driver, url, user, password)
  }
}
