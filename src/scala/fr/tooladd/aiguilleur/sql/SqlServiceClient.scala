/**
  * Copyright (C) 2019 Tool-Add
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
  */

package fr.tooladd.aiguilleur.sql

import akka.actor.Cancellable
import akka.stream.scaladsl.Source
import com.typesafe.config.Config
import fr.tooladd.aiguilleur.{AiguilleurRequest, AiguilleurServiceClient, AiguilleurServiceConnector}
import scalikejdbc.{NoExtractor, SQL}

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration
import scala.util.Try


class SqlServiceClient(val describer: SqlServiceDescriber) extends AiguilleurServiceClient[String, SQL[Nothing, NoExtractor], SqlServiceDescriber] {

  def poll[aReq <: AiguilleurRequest, aRes](
    pollInterval: FiniteDuration,
    request: aReq
  )( implicit
    rsc: AiguilleurServiceConnector[aReq, aRes, String, SQL[Nothing, NoExtractor], SqlServiceDescriber]
  ): Source[(Try[aRes], aReq), Cancellable] = ???


  def singleRequest[aReq <: AiguilleurRequest, aRes](
    request: aReq
  )( implicit
    asc: AiguilleurServiceConnector[aReq, aRes, String, SQL[Nothing, NoExtractor], SqlServiceDescriber]
  ): Future[aRes] = {
    val sqlRequest = asc.buildServiceRequest(describer, request)
    val response: SQL[Nothing, NoExtractor] = SQL(sqlRequest)
    val result = asc.handleServiceResponse(describer, response, request)
    Future.successful(result)
  }

}


object SqlServiceClient {

  def apply(config: Config): SqlServiceClient = {
    new SqlServiceClient(SqlServiceDescriber(config))
  }

}

