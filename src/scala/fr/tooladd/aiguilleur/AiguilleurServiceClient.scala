package fr.tooladd.aiguilleur

import akka.actor.Cancellable
import akka.stream.scaladsl.Source

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration
import scala.util.Try

/**
  *
  */
trait AiguilleurServiceClient[sReq, sRes, aDesc <: AiguilleurServiceDescriber] {

  def describer: aDesc

  def poll[aReq <: AiguilleurRequest, aRes](
    pollInterval: FiniteDuration,
    request: aReq
  )( implicit
    rsc: AiguilleurServiceConnector[aReq, aRes, sReq, sRes, aDesc]
  ): Source[(Try[aRes], aReq), Cancellable]

  def singleRequest[aReq <: AiguilleurRequest, aRes](
    request: aReq
  )( implicit
    rsc: AiguilleurServiceConnector[aReq, aRes, sReq, sRes, aDesc]
  ): Future[aRes]

}
