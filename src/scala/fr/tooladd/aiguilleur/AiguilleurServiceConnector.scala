/**
  * Copyright (C) 2019 Tool-Add
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
  */

package fr.tooladd.aiguilleur

import scala.concurrent.Future

trait AiguilleurServiceConnector[aReq <: AiguilleurRequest, aRes, sReq, sRes, aDesc <: AiguilleurServiceDescriber] {

  def buildServiceRequest (
    describer : aDesc,
    req: aReq
  ): sReq

  def handleServiceResponse (
    describer : aDesc,
    serviceResponse: sRes,
    req: aReq
  ): aRes

  def handleServiceResponse (
    describer : aDesc,
    serviceResponse: Future[sRes],
    req: aReq
  ): Future[aRes]

}

object AiguilleurServiceConnector {

  implicit class AiguilleurServiceConnectorOps[
    aReq <: AiguilleurRequest,
    aRes,
    sReq,
    sRes,
    aDesc <: AiguilleurServiceDescriber
  ] (req: aReq) {

    def buildServiceRequest ( describer: aDesc )(
      implicit asc: AiguilleurServiceConnector[aReq, aRes, sReq, sRes, aDesc]
    ): sReq =
      asc.buildServiceRequest(describer, req)

    def handleServiceResponse( describer: aDesc, sResponse: sRes )(
      implicit asc: AiguilleurServiceConnector[aReq, aRes, sReq, sRes, aDesc]
    ): aRes = asc.handleServiceResponse(describer, sResponse, req)

    def handleServiceResponse ( describer: aDesc, serviceResponse: Future[sRes] )(
      implicit asc: AiguilleurServiceConnector[aReq, aRes, sReq, sRes, aDesc]
    ): Future[aRes] = asc.handleServiceResponse(describer, serviceResponse, req)

  }

}
