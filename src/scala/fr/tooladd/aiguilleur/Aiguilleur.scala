/**
  * Copyright (C) 2019 Tool-Add
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
  */

package fr.tooladd.aiguilleur

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.typesafe.config.Config
import fr.tooladd.aiguilleur.http.HttpServiceClient
import fr.tooladd.aiguilleur.sql.SqlServiceClient
import akka.stream.Materializer
import wvlet.log.LogSupport


case object Aiguilleur extends LogSupport {

  private var _system: Option[ActorSystem] = None
  private var _mat: Option[Materializer] = None

  // Getters
  def system = _system match {
    case Some(system) => system
    case None => {
      warn("ActorSystem not given to Aiguilleur, it is suspicious that we have to create a new one.")
      _system = Some(ActorSystem("Aiguilleur"))
      _system.get
    }
  }
  def materializer = _mat match {
    case Some(mat) => mat
    case None => {
      _mat = Some(ActorMaterializer()(system))
      _mat.get
    }
  }

  // Setters
  def system_= (system:ActorSystem):Unit = _system = {
    info(s"ActorSystem '${system.name}' given to Aiguilleur.")
    Some(system)
  }
  def materializer_= (mat:Materializer):Unit = _mat = Some(mat)


  def apply(config: Config) : Object = {

    val serviceType = config.getString("service-type")
    val serviceConf = config.getConfig("service-conf")

    serviceType match {
      case "REST" => HttpServiceClient(serviceConf)(system, materializer)
      case "MARIADB" => SqlServiceClient(serviceConf)
      case _ => ???
    }
  }

}




