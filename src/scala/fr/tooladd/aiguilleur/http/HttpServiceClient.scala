/**
  * Copyright (C) 2019 Tool-Add
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
  */

package fr.tooladd.aiguilleur.http

import akka.actor.{ActorSystem, Cancellable}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest => AkkaHttpRequest, _}
import akka.stream.Materializer
import akka.stream.scaladsl.Source
import com.typesafe.config.Config
import fr.tooladd.aiguilleur.{AiguilleurRequest, AiguilleurServiceClient, AiguilleurServiceConnector}

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration
import scala.util.{Failure, Success, Try}


/**
  * @param describer
  * @param system
  * @param mat
  *
  * TODO :
  *   * add proper logging system
  *   * review errors / exceptions management
  */
class HttpServiceClient(val describer: HttpServiceDescriber)( implicit
  system: ActorSystem,
  mat: Materializer
) extends AiguilleurServiceClient[AkkaHttpRequest, HttpResponse, HttpServiceDescriber] {

  private def filterSuccessResponse[Req](input: (Try[HttpResponse], Req))
    (implicit mat: Materializer): (Try[HttpResponse], Req) = input match {
    case (Success(response), request) => {

      val (returnedElem, message): (Try[HttpResponse], String) =

        // Service answers with a 2xx status code
        if (response.status.isSuccess) {
          (Success(response), s"Service responded with http status code ${response.status} to http poll request.")
        } else {
          response.discardEntityBytes(mat) // TODO keep error message for logs
          val tmpMsg = s"Service responded with http status code ${response.status} to http poll request"
          (Failure(
            new IllegalResponseException(
              new ErrorInfo(summary         = response.status.defaultMessage, detail = tmpMsg, errorHeaderName = response.status.value)
            )
          ),
            tmpMsg
          )

        }

      (returnedElem, request)
    }

    case (Failure(res), req) => {
      (Failure(res), req)
    }
  }

  def singleRequest[Req <: AiguilleurRequest, Res](
    request: Req
  )( implicit
    rsc: AiguilleurServiceConnector[Req, Res, AkkaHttpRequest, HttpResponse, HttpServiceDescriber]
  ): Future[Res] = {
    val httpRequest = rsc.buildServiceRequest(describer, request)
    val futureResponse: Future[HttpResponse] =
      Http().singleRequest(httpRequest, settings = describer.connectionSettings, connectionContext = describer.connectionCtx)
    rsc.handleServiceResponse(describer, futureResponse, request)
  }


  def poll[aReq <: AiguilleurRequest, aRes](
    pollInterval: FiniteDuration,
    request: aReq
  )( implicit
    rsc: AiguilleurServiceConnector[aReq, aRes, AkkaHttpRequest, HttpResponse, HttpServiceDescriber]
  ): Source[(Try[aRes], aReq), Cancellable] = ???

}


object HttpServiceClient {

  def apply(config: Config)(
    implicit
    system: ActorSystem,
    mat:    Materializer
  ): HttpServiceClient = {
    new HttpServiceClient(HttpServiceDescriber(config))
  }

}
