/**
  * Copyright (C) 2019 Tool-Add
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
  */

package fr.tooladd.aiguilleur.http

import java.net.InetSocketAddress
import java.nio.file.Paths

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.settings.{ClientConnectionSettings, ConnectionPoolSettings}
import akka.http.scaladsl.{ClientTransport, Http, HttpsConnectionContext}
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.sslconfig.akka.AkkaSSLConfig
import com.typesafe.sslconfig.ssl.{SSLConfigFactory, SSLConfigSettings}
import fr.tooladd.aiguilleur.AiguilleurServiceDescriber

case class HttpServiceDescriber(
  scheme:             String,
  host:               String,
  port:               Option[Int],
  apiBasepath:        Option[String],
  connectionSettings: ConnectionPoolSettings,
  connectionCtx:      HttpsConnectionContext,
  queueSize:          Int                    = 10
) extends AiguilleurServiceDescriber {

  private val _port: Int = port.getOrElse(
    scheme match {
      case "http" => 80
      case "https" => 443
      case _ => ???
    }
  )

  private val _url: Uri = Uri.from(scheme = scheme, host = host, port = _port)

  def getPath(relativePath: String = ""): String =  _url.toString() + Paths.get("/", apiBasepath.getOrElse(""), relativePath)

}



// TODO : review and test ssl configuration
object HttpServiceDescriber {

  val sslConfigPath = "ssl-config"

  // @TODO review the way to build connection context from https://doc.akka.io/docs/akka/2.4.7/scala/http/client-side/client-https-support.html#clientsidehttps
  def getConnectionContext(config: Config)(implicit system: ActorSystem): HttpsConnectionContext = {

    val httpClient = Http()


    val sslSourceConfig = if(config.hasPath(sslConfigPath)) {
      config.getConfig(sslConfigPath)
    } else {
      ConfigFactory.parseString("")
    }

    val akkaSslConfig = new AkkaSSLConfig(httpClient.system, getSSLConfigSettings(sslSourceConfig))
    httpClient.createClientHttpsContext(akkaSslConfig)

  }

  /**
    * Default store behavior is to consider given string as subconfiguration object. If nothing given,
    * it will ensure default akka / java store management.
    *
    * @param config
    * @param system
    * @return
    */
  private def getSSLConfigSettings(config: Config)(implicit system: ActorSystem): SSLConfigSettings = {

    val sslConfig = if (config.hasPath("trustManager.stores")) {
      val storeConf = ConfigFactory.parseString(config.getString("trustManager.stores"))
      config.withValue("trustManager.stores", storeConf.getList("stores"))
    } else config.withoutPath("trustManager.stores")

    val defaultSslConfig = system.settings.config.getConfig(sslConfigPath)
    SSLConfigFactory.parse(sslConfig withFallback defaultSslConfig)
  }

  private def getProxyConfig(sslConfig: Config, scheme: String, proxyKey: String): Option[String] = {
    val proxyAttribute =
      if (sslConfig.hasPath(s"proxy.${proxyKey}")) sslConfig.getString(s"proxy.${proxyKey}")
      else System.getProperty(s"${scheme}.proxy${proxyKey.capitalize}")

    if (proxyAttribute == null || proxyAttribute.isEmpty) None
    else Some(proxyAttribute)
  }

  @deprecated
  private def getConnectionSettings(config: Config)(implicit system: ActorSystem): ConnectionPoolSettings = {

    val scheme = config.getString("uri.scheme")

    if(config.hasPath(sslConfigPath)) {

      // TODO : clean this code => default ssl config to review.

      val sslConfig = config.getConfig(sslConfigPath)
      val proxyHost = getProxyConfig(sslConfig, scheme, "host")
      val proxyPort = getProxyConfig(sslConfig, scheme, "port").map(_.toInt)

      if (proxyHost.isDefined && proxyPort.isDefined) {

        val proxy = InetSocketAddress.createUnresolved(proxyHost.get, proxyPort.get)
        val proxyTransport = ClientTransport.httpsProxy(proxy)

        ConnectionPoolSettings(system).withConnectionSettings(
          ClientConnectionSettings(system).withTransport(proxyTransport)
        )

      } else {
        // don't use proxy if parameters not given properly
        ConnectionPoolSettings(system)
      }

    } else {
      ConnectionPoolSettings(system)
    }

  }

  // TODO search how to remove implicit ActorSystem
  def apply(config: Config)(implicit system: ActorSystem): HttpServiceDescriber = {
    val scheme = config.getString("uri.scheme")
    val host = config.getString("uri.host")
    val port = if(config.hasPath("uri.port")) Some(config.getInt("uri.port")) else None
    val apiBasepath = if(config.hasPath("uri.api-basepath")) Some(config.getString("uri.api-basepath")) else None
    new HttpServiceDescriber(scheme, host, port, apiBasepath, getConnectionSettings(config), getConnectionContext(config))
  }
}
